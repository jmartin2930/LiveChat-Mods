<!DOCTYPE html>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<title>Created!</title>
</head>
<body>
<div id="main">
<div id="main_content">
<h1>Done! Awesome!</h1>
<?php
$domain = $_SERVER['SERVER_NAME'];
$jomax = $_POST['jomax'];
$background_color = $_POST['background_color'];
$agent_name = $_POST['agent_name'];
$agent_font = $_POST['agent_font'];
$agent_background = $_POST['agent_background'];
$visitor_name = $_POST['visitor_name'];
$visitor_font = $_POST['visitor_font'];
$visitor_background = $_POST['visitor_background'];

$myfile = fopen("custom/$jomax.css", "w") or die("Unable to open file!");
$txt = "/*GENERAL SETTINGS*/\n/*Changes background and body text*/\nbody {background-color: $background_color !important; color: #9AA !important;}\n/*Changes the color of the survey background and font color of survey answers*/\n.survey {background-color: #fff !important; color: #9AA !important;}\n/*Changes the color of the survey question font*/\n.survey span {color: #9AA !important;}\n/*Adds an underline to the message box*/\n.message {border-bottom: 1px solid #000;}\n/*AGENT SETTINGS*/\n/*Changes the color of your name*/\n.chat-window .chat-content .message.own .author, .supervise-window .chat-content .message .author {color: $agent_name !important;}\n/*Changes the color of your Font*/\n.chat-window .chat-content .message.own .text, .supervise-window .chat-content .message .text   {color: $agent_font !important;}\n/*Changes the background color of the chat agent's rows*/\n.chat-window .chat-content .message.integration, .chat-window .chat-content .message.own, .chat-window .chat-content .message.supervisor, .chat-window .chat-content .message.system, .supervise-window .chat-content .message.integration, .supervise-window .chat-content .message.own, .supervise-window .chat-content .message.supervisor, .supervise-window .chat-content .message.system {background-color: $agent_background !important;}\n/*VISITOR SETTINGS*/\n/*Changes the color of the visitor name*/\n.chat-window .chat-content .message.visitor .author, .supervise-window .chat-content .message .author {color: $visitor_name !important;}\n/*Changes the color of the visitor's Font*/\n.chat-window .chat-content .message.visitor .text, .supervise-window .chat-content .message .text {color: $visitor_font !important;}\n/*Changes the background color of the visitor's rows rows*/\n.chat-window .chat-content .message, .supervise-window .chat-content .message {background-color: $visitor_background !important;}";
fwrite($myfile, $txt);
fclose($myfile);
  
$myjs = fopen("custom/$jomax.user.js", "w") or die("Unable to open file!");
$txt = "// ==UserScript==//\n// @name        livechat_mods_$jomax\n// @namespace   livechat_mods\n// @include     https://my.livechatinc.com\n// @match       https://my.livechatinc.com/*\n// @grant       none\n// ==/UserScript==\n";
fwrite($myjs, $txt);
$txt = "var link = window.document.createElement('link');\nlink.rel = 'stylesheet';\nlink.type = 'text/css';\nlink.href = 'https://$domain/livechat/custom/$jomax.css';\ndocument.getElementsByTagName('HEAD')[0].appendChild(link);";
fwrite($myjs, $txt);
fclose($myjs);
?>
  
<h2>Custom for <?php echo $_POST['jomax']; ?></h2>
<table>
<tr>
<td>GreaseMonkey</td>
<td><a href="custom/<?php echo $jomax; ?>.user.js"><i class="fa fa-download"></i></a></td>
</tr>
<tr>
<td>Stylish</td>
<td><a href="custom/<?php echo $jomax; ?>.css"><i class="fa fa-download"></i></a></td>
</tr>
</table>
<div id="demo" style="background: <?php echo $background_color; ?>;">
<span style="color:<?php echo $agent_name; ?>; background-color:<?php echo $agent_background; ?>;">Agent name</span><br>
<span style="color:<?php echo $agent_font; ?>; background-color:<?php echo $agent_background; ?>;">Agent Text</span><br>
<span style="color:<?php echo $visitor_name; ?>; background-color:<?php echo $visitor_background; ?>;">Visitor Name</span><br>
<span style="color:<?php echo $visitor_font; ?>; background-color:<?php echo $visitor_background; ?>;">Visitor Text</span><br>
</div>
  
<br><br>
</div>
</div>
  
<div id="footer">
  <?PHP echo $_SERVER['SERVER_NAME'] ?> | 2013-<?PHP echo date("Y"); ?> | <a href="humans.txt" target="_blank">Contributors</a>
</div>
  
<?
/*
$background_color
$agent_name
$agent_font
$agent_background
$visitor_name
$visitor_font
$visitor_background
*/
?>