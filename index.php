<!DOCTYPE html>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<title>LiveChat Mods</title>
</head>
<body>
<div id="main">
<div id="main_content">
<h1>LiveChat Mods</h1>
Welcome to LiveChat Mods. From here you can customize your livechat expirience to get the colors you want. We are always working to improve our scripts, so feedback is welcome!

<h2>How do I use these scripts?</h2>
You can use the GreaseMonkey scripts on <b>Chrome</b> or <b>Firefox</b> with Addons. Chrome uses <a href="https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en">TamperMonkey</a>, and Firefox uses <a href="https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/">GreaseMonkey</a>. <a href="https://userstyles.org/">Stylish</a> will work on either browser.
<br><br>
Click the <i class="fa fa-download" style="color: #0076DE;"></i> to download/view.
<hr>  

<h2>The Basic</h2>
<i class="update">Last update: 02-10-2016</i></br>
<table>
<tr>
<td>GreaseMonkey</td>
<td><a href="scripts/basic.user.js"><i class="fa fa-download"></i></a></td>
</tr>
<tr>
<td>Stylish</td>
<td><a href="mods/basic.css"><i class="fa fa-download"></i></a></td>
</tr>
</table>
<div id="demo" style="background: #fff;">
<span style="color:#78C043; background-color: #E8E8E8;">Agent name</span><br>
<span style="color:#666666; background-color: #E8E8E8;">Agent Text</span><br>
<span style="color:#F06A2D; background-color: #D1D1D1;">Visitor Name</span><br>
<span style="color:#666666; background-color: #D1D1D1;">Visitor Text</span><br>
</div>

<h2>ColorBlind</h2>
</table>
<i class="update">Last update: 02-15-2016</i></br>
<table>
<tr>
<td>GreaseMonkey</td>
<td><a href="scripts/colorblind.user.js"><i class="fa fa-download"></i></a></td>
</tr>
<tr>
<td>Stylish</td>
<td><a href="mods/colorblind.css"><i class="fa fa-download"></i></a></td>
</tr>
</table>
<div id="demo" style="background: #333333;">
<span style="color:#0000FF; background-color: #A8A8A8;">Agent name</span><br>
<span style="color:#009900; background-color: #A8A8A8;">Agent Text</span><br>
<span style="color:#CC0000; background-color: #A8A8A8;">Visitor Name</span><br>
<span style="color:#9600BF; background-color: #A8A8A8;">Visitor Text</span><br>
</div>

<h2>Goth</h2>
<i class="update">Last update: 02-15-2016</i></br>
<table>
<tr>
<td>GreaseMonkey</td>
<td><a href="scripts/goth.user.js"><i class="fa fa-download"></i></a></td>
</tr>
<tr>
<td>Stylish</td>
<td><a href="mods/goth.css"><i class="fa fa-download"></i></a></td>
</tr>
</table>
<div id="demo" style="background: #000;">
<span style="color:#000000;background-color: #595959;">Agent name</span><br>
<span style="color:#DEDEDE;background-color: #595959;">Agent Text</span><br>
<span style="color:#FF8800;background-color: #828282;">Visitor Name</span><br>
<span style="color:#B50000;background-color: #828282;">Visitor Text</span><br>
</div>
  
<h2>Atom</h2>
<i class="update">Last update: 02-10-2016</i></br>
<table>
<tr>
<td>GreaseMonkey</td>
<td><a href="scripts/atom.user.js"><i class="fa fa-download"></i></a></td>
</tr>
<tr>
<td>Stylish</td>
<td><a href="mods/atom.css"><i class="fa fa-download"></i></a></td>
</tr>
</table>
<div id="demo" style="background: #2C323C;">
<span style="color:#E06C60;background-color: #282C34;">Agent name</span><br>
<span style="color:#CB8145;background-color: #282C34;">Agent Text</span><br>
<span style="color:#52A679;background-color: #282C34;">Visitor Name</span><br>
<span style="color:#CB8145;background-color: #282C34;">Visitor Text</span><br>
</div>
  
<h2>Custom</h2>
If the options above don't suite you, that's okay, try our custom generation form. This will create a server side file for you that you can link to in GreaseMonkey/TamperMonkey or copy for Stylish.<br>
<br>
<a href="http://www.colorpicker.com/" target="_blank">Click here</a> to see a color picking tool.<br>
<form method="post" action="custom_create.php">
Jomax ID:<br>
<input name="jomax" type="text" placeholder="Example: bparsons" required>
Page background color (in HEX):<br>
<input name="background_color" type="text" placeholder="#000000" required>
Agent name text color (in HEX):<br>
<input name="agent_name" type="text" placeholder="#000000" required>
Agent response text color (in HEX):<br>
<input name="agent_font" type="text" placeholder="#000000" required>
Agent box background color (in HEX):<br>
<input name="agent_background" type="text" placeholder="#000000" required>
Visitor name text color (in HEX):<br>
<input name="visitor_name" type="text" placeholder="#000000" required>
Visitor response text color (in HEX):<br>
<input name="visitor_font" type="text" placeholder="#000000" required>
Visitor box background color (in HEX):<br>
<input name="visitor_background" type="text" placeholder="#000000" required>
<input type="submit" value="Generate">
<br>
</div>
</body>
</div>
</div>

<div id="footer">
  <?PHP echo $_SERVER['SERVER_NAME'] ?> | 2013-<?PHP echo date("Y"); ?> | <a href="humans.txt" target="_blank">Contributors</a>
</div>

</html>