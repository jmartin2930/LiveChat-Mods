// ==UserScript==
// @name        livechat_mods_colorblind
// @namespace   livechat_mods
// @include     https://my.livechatinc.com
// @match       https://my.livechatinc.com/*
// @version     .1
// @grant       none
// ==/UserScript==

    var link = window.document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://planetchat.xyz/livechat/mods/colorblind.css';
    document.getElementsByTagName("HEAD")[0].appendChild(link);
